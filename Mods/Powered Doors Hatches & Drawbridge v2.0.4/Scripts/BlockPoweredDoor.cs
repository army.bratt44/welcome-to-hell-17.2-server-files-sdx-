using System;
using UnityEngine;
using Audio;
using GUI_2;

public class BlockPoweredDoor : BlockDoorSecure
{
	private BlockPoweredDoor blockPoweredDoor;
	private DateTime dteNextToolTipDisplayTime;
	private DoorAutoTrigger script;
	private Animator autoAnimator;
	private static bool showDebugLog = false;
	private bool removingDoor;
	
	public static void DebugMsg(string msg)
	{
		if(showDebugLog)
		{
			Debug.Log(msg);
		}
	}
	
	public static bool IsHatch(Block block)
	{
		bool isHatch = false;
		if (block.Properties.Values.ContainsKey("Hatch"))
        {
			bool.TryParse(block.Properties.Values["Hatch"], out isHatch);
        }
		return isHatch;
	}
	
	private int TopInputPos()
	{
		int topInputPos = 1;
		if (this.Properties.Values.ContainsKey("TopInput"))
        {
			int.TryParse(this.Properties.Values["TopInput"], out topInputPos);
        }
		if(topInputPos >= 4)
		{
			topInputPos = 4;
		}
		return topInputPos;
	}
	
	private int BottomInputPos()
	{
		int bottomInputPos = 2;
		if (this.Properties.Values.ContainsKey("BottomInput"))
        {
			int.TryParse(this.Properties.Values["BottomInput"], out bottomInputPos);
        }
		if(bottomInputPos >= 4)
		{
			bottomInputPos = 4;
		}
		return bottomInputPos;
	}
	
	public bool IsDoorControl(Block block)
	{
		bool isDoorControl = false;
		if (block.Properties.Values.ContainsKey("DoorController"))
        {
			bool.TryParse(block.Properties.Values["DoorController"], out isDoorControl);
        }
		return isDoorControl;
	}
	
	public bool IsNormallyOpened(WorldBase _world)
	{
		bool isNormallyOpened = false;
		if (!_world.IsRemote())
		{
			if (this.Properties.Values.ContainsKey("ClosedOnPowered"))
			{
				bool.TryParse(this.Properties.Values["ClosedOnPowered"], out isNormallyOpened);
			}
		}
		return isNormallyOpened;
	}
	
	public bool AutoTrigger()
	{
		bool isAutoTriggerOn = false;
		if (this.Properties.Values.ContainsKey("AutoTrigger"))
        {
			bool.TryParse(this.Properties.Values["AutoTrigger"], out isAutoTriggerOn);
        }
		return isAutoTriggerOn;
	}
	public bool Allow4Inputs()
	{
		bool allow4Inputs = false;
		if (this.Properties.Values.ContainsKey("4Inputs"))
        {
			bool.TryParse(this.Properties.Values["4Inputs"], out allow4Inputs);
        }
		return allow4Inputs;
	}
	
	// Block
	public override void OnBlockEntityTransformBeforeActivated(WorldBase _world, Vector3i _blockPos, int _cIdx, BlockValue _blockValue, BlockEntityData _ebcd)
	{
		BlockPoweredDoor.DebugMsg("OnBlockEntityTransformBeforeActivated");
		this.shape.OnBlockEntityTransformBeforeActivated(_world, _blockPos, _cIdx, _blockValue, _ebcd);
		if(AutoTrigger())
		{
			blockPoweredDoor = this;
			if(_ebcd != null && _ebcd.bHasTransform)
			{
				GameObject gameObject = _ebcd.transform.gameObject;
				if(gameObject == null)
				{
					BlockPoweredDoor.DebugMsg("gameObject null (OnBlockEntityTransformBeforeActivated)");
				}
				script = gameObject.GetComponent<DoorAutoTrigger>();
				if(script == null)
				{
					BlockPoweredDoor.DebugMsg("Adding script");
					script = gameObject.AddComponent<DoorAutoTrigger>();
					if(script == null)
					{
						BlockPoweredDoor.DebugMsg("Failed to add script");
						return;
					}
				}
				BlockPoweredDoor.DebugMsg("Script found/added");
				script.enabled = true;
				script.blockPos = _blockPos;
				script.blockValue = _blockValue;
				script.cIdx = _cIdx;
				script.ebcd = ((World)_world).ChunkClusters[_cIdx].GetBlockEntity(script.blockPos);
				script.normallyOpened = IsNormallyOpened(_world);
			}
			else
			{
				BlockPoweredDoor.DebugMsg("_ebcd is null (OnBlockEntityTransformBeforeActivated)");
				return;
			}
		}
	}
	
	public Animator DoorAnimator(BlockEntityData blockEntityData)
	{
		Animator doorAnimator = null;
		Animator[] componentsInChildren;
		if (blockEntityData != null && blockEntityData.bHasTransform && (componentsInChildren = blockEntityData.transform.GetComponentsInChildren<Animator>()) != null)
		{
			Animator[] array = componentsInChildren;
			for (int i = 0; i < array.Length; i++)
			{
				Animator animator = array[i];
				doorAnimator = animator;
			}
		}
		float animSpeed = 1;
		if (this.Properties.Values.ContainsKey("AnimationSpeed"))
		{
			float.TryParse(this.Properties.Values["AnimationSpeed"], out animSpeed);
		}
		doorAnimator.speed = animSpeed;
		autoAnimator = doorAnimator;
		return doorAnimator;
	}
	
	
	public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
	{
		if(AutoTrigger())
		{
			return false;
		}
		if (_blockValue.ischild)
		{
			Vector3i parentPos = Block.list[_blockValue.type].multiBlockPos.GetParentPos(_blockPos, _blockValue);
			BlockValue block = _world.GetBlock(parentPos);
			return this.OnBlockActivated(_indexInBlockActivationCommands, _world, _cIdx, parentPos, block, _player);
		}
		if(BlockPoweredDoor.HasActivePower(_world, _cIdx, _blockPos))
		{
			BlockPoweredDoor.DebugMsg("Block has active power.");
		}
		else
		{
			DisplayToolTipText("The door has no power.");
			Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/locked");
			return false;
		}
		TileEntitySecureDoor tileEntitySecureDoor = (TileEntitySecureDoor)_world.GetTileEntity(_cIdx, _blockPos);
		if (tileEntitySecureDoor == null && !_world.IsEditor())
		{
			return false;
		}
		bool flag = _world.IsEditor() ? ((_blockValue.meta & 4) != 0) : tileEntitySecureDoor.IsLocked();
		bool flag2 = !_world.IsEditor() && tileEntitySecureDoor.IsUserAllowed(GamePrefs.GetString(EnumGamePrefs.PlayerId));
		switch (_indexInBlockActivationCommands)
		{
		case 0:
			if (!_world.IsEditor() && flag && !flag2)
			{
				Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/locked");
				return false;
			}
			return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
		case 1:
			if (!_world.IsEditor() && flag && !flag2)
			{
				Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/locked");
				return false;
			}
			return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
		case 2:
			if (_world.IsEditor())
			{
				_blockValue.meta |= 4;
				_world.SetBlockRPC(_cIdx, _blockPos, _blockValue);
			}
			else
			{
				tileEntitySecureDoor.SetLocked(true);
			}
			Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/locking");
			GameManager.ShowTooltip(_player as EntityPlayerLocal, "doorLocked");
			return true;
		case 3:
			if (_world.IsEditor())
			{
				_blockValue.meta = (byte)((int)_blockValue.meta & -5);
				_world.SetBlockRPC(_cIdx, _blockPos, _blockValue);
			}
			else
			{
				tileEntitySecureDoor.SetLocked(false);
			}
			Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/unlocking");
			GameManager.ShowTooltip(_player as EntityPlayerLocal, "doorUnlocked");
			return true;
		case 4:
		{
			LocalPlayerUI uIForPlayer = LocalPlayerUI.GetUIForPlayer(_player as EntityPlayerLocal);
			if (uIForPlayer != null)
			{
				XUiC_KeypadWindow.Open(uIForPlayer, tileEntitySecureDoor);
			}
			// NGuiKeypad.Instance.LockedItem = tileEntitySecureDoor;
			return true;
		}
		default:
			return false;
		}
	}
	
	public static bool HasActivePower(WorldBase _world, int _cIdx, Vector3i _blockPos)
	{
		if(_world == null)
		{
			_world = GameManager.Instance.World;
			if(_world == null)
			{
				BlockPoweredDoor.DebugMsg("_world is still null (HasActivePower)");
			}
		}
		BlockValue blockValue = GameManager.Instance.World.GetBlock(_blockPos);
		Block block = Block.list[blockValue.type];
		BlockPoweredDoor doorBlock = block as BlockPoweredDoor;
		if(doorBlock == null)
		{
			//DONT REMOVE THIS RETURN, THIS STOPS NULL REF ON REMOVE
			BlockPoweredDoor.DebugMsg("doorBlock null HasActivePower will return false");
			return false;
		}
		if(doorBlock.removingDoor)
		{
			BlockPoweredDoor.DebugMsg("doorBlock.removingDoor true HasActivePower will return false");
			return false;
		}
		if(BlockPoweredDoor.IsHatch(block))
		{
			return HatchHasPower(_world, _cIdx, _blockPos);
		}
		int doorSizeOnY = 2;
		if (block.Properties.Values.ContainsKey("MultiBlockDim"))
		{
			Vector3i dim = StringParsers.ParseVector3i(block.Properties.Values[Block.PropMultiBlockDim], 0, -1, false);
			doorSizeOnY = dim.y;
		}
		Vector3i inputPosA = new Vector3i(_blockPos.x,_blockPos.y+doorSizeOnY-1+doorBlock.TopInputPos(),_blockPos.z);
		Vector3i inputPosB = new Vector3i(_blockPos.x,_blockPos.y-doorBlock.BottomInputPos(),_blockPos.z);
		Vector3i inputPosC = new Vector3i(_blockPos.x,inputPosA.y+1,_blockPos.z);
		Vector3i inputPosD = new Vector3i(_blockPos.x,inputPosB.y-1,_blockPos.z);
		bool allow4Inputs = doorBlock.Allow4Inputs(); // maybe change Allow4Inputs() to an int....
		int inputs = 2;
		if(allow4Inputs)
		{
			inputs = 4;
			
		}
		Vector3i[] locations = new Vector3i[inputs];
		if(allow4Inputs)
		{
			locations[0] = inputPosA;
			locations[1] = inputPosB;
			locations[2] = inputPosC;
			locations[3] = inputPosD;
		}
		else
		{
			locations[0] = inputPosA;
			locations[1] = inputPosB;
		}
		foreach (Vector3i inputPos in locations)
		{
			BlockValue inputBlockValue = _world.GetBlock(inputPos);
			Block inputBlock = Block.list[inputBlockValue.type];
			Type inputBlockType = Block.list[inputBlockValue.type].GetType();
			if(inputBlockType == typeof(BlockPowered))
			{
				TileEntityPowered tileEntityPowered = (TileEntityPowered)_world.GetTileEntity(_cIdx, inputPos);
				if (tileEntityPowered != null)
				{
					if(tileEntityPowered.IsPowered)
					{
						if(doorBlock.IsDoorControl(inputBlock))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public static bool HatchHasPower(WorldBase _world, int _cIdx, Vector3i _blockPos)
	{
		bool hatch = false;
		if(_world == null)
		{
			_world = GameManager.Instance.World;
			if(_world == null)
			{
				BlockPoweredDoor.DebugMsg("_world is still null (HatchHasPower)");
			}
		}
		BlockValue blockValue = GameManager.Instance.World.GetBlock(_blockPos);
		Block block = Block.list[blockValue.type];
		BlockPoweredDoor doorBlock = block as BlockPoweredDoor;
		if(doorBlock == null)
		{
			// DONT REMOVE THIS RETURN, THIS STOPS NULL REF ON REMOVE
			// the door has been removed
			return false;
		}
		if(doorBlock.removingDoor)
		{
			return false;
		}
		Vector3i newBlockPos = new Vector3i(_blockPos.x,_blockPos.y-1,_blockPos.z);
		Vector3i inputPosA = new Vector3i(newBlockPos.x+1,newBlockPos.y,newBlockPos.z);
		Vector3i inputPosB = new Vector3i(newBlockPos.x-1,newBlockPos.y,newBlockPos.z);
		Vector3i inputPosC = new Vector3i(newBlockPos.x,newBlockPos.y,newBlockPos.z+1);
		Vector3i inputPosD = new Vector3i(newBlockPos.x,newBlockPos.y,newBlockPos.z-1);
		
		Vector3i[] locations = new Vector3i[4];
		locations[0] = inputPosA;
		locations[1] = inputPosB;
		locations[2] = inputPosC;
		locations[3] = inputPosD;
		foreach (Vector3i inputPos in locations)
		{
			BlockValue inputBlockValue = _world.GetBlock(inputPos);
			Block inputBlock = Block.list[inputBlockValue.type];
			Type inputBlockType = Block.list[inputBlockValue.type].GetType();
			if(inputBlockType == typeof(BlockPowered))
			{
				TileEntityPowered tileEntityPowered = (TileEntityPowered)_world.GetTileEntity(_cIdx, inputPos);
				if (tileEntityPowered != null)
				{
					if(tileEntityPowered.IsPowered)
					{
						if(doorBlock.IsDoorControl(inputBlock))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
	{
		if(AutoTrigger())
		{
			return "Testing";
		}
		else
		{
			if (_blockValue.ischild)
			{
				Vector3i parentPos = Block.list[_blockValue.type].multiBlockPos.GetParentPos(_blockPos, _blockValue);
				BlockValue block = _world.GetBlock(parentPos);
				return this.GetActivationText(_world, block, _clrIdx, parentPos, _entityFocusing);
			}
			TileEntitySecureDoor tileEntitySecureDoor = (TileEntitySecureDoor)_world.GetTileEntity(_clrIdx, _blockPos);
			if (tileEntitySecureDoor == null && !_world.IsEditor())
			{
				return string.Empty;
			}
			bool flag = _world.IsEditor() ? ((_blockValue.meta & 4) != 0) : tileEntitySecureDoor.IsLocked();
			PlayerActionsLocal playerInput = ((EntityPlayerLocal)_entityFocusing).playerInput;
			string keybindString = UIUtils.GetKeybindString(playerInput.Activate, playerInput.PermanentActions.Activate);
			string arg = Localization.Get("door", string.Empty);
			if (!flag)
			{
				return string.Format(Localization.Get("tooltipUnlocked", string.Empty), keybindString, arg);
			}
			return string.Format(Localization.Get("tooltipLocked", string.Empty), keybindString, arg);
		}
		
	}
	
	public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
	{
		if(AutoTrigger())
		{
			return new BlockActivationCommand[0];
		}
		if (_blockValue.ischild)
		{
			Vector3i parentPos = Block.list[_blockValue.type].multiBlockPos.GetParentPos(_blockPos, _blockValue);
			BlockValue block = _world.GetBlock(parentPos);
			return this.GetBlockActivationCommands(_world, block, _clrIdx, parentPos, _entityFocusing);
		}
		TileEntitySecureDoor tileEntitySecureDoor = (TileEntitySecureDoor)_world.GetTileEntity(_clrIdx, _blockPos);
		if (tileEntitySecureDoor == null && !_world.IsEditor())
		{
			return new BlockActivationCommand[0];
		}
		string @string = GamePrefs.GetString(EnumGamePrefs.PlayerId);
		PersistentPlayerData persistentPlayerData = _world.IsEditor() ? null : _world.GetGameManager().GetPersistentPlayerList().GetPlayerData(tileEntitySecureDoor.GetOwner());
		bool flag = _world.IsEditor() || (!tileEntitySecureDoor.IsOwner(@string) && (persistentPlayerData != null && persistentPlayerData.ACL != null) && persistentPlayerData.ACL.Contains(@string));
		bool flag2 = _world.IsEditor() ? ((_blockValue.meta & 4) != 0) : tileEntitySecureDoor.IsLocked();
		bool flag3 = _world.IsEditor() || tileEntitySecureDoor.IsOwner(@string);
		bool flag4 = !_world.IsEditor() && tileEntitySecureDoor.IsUserAllowed(@string);
		bool flag5 = !_world.IsEditor() && tileEntitySecureDoor.HasPassword();
		this.AZ[0].enabled = BlockDoor.IsDoorOpen(_blockValue.meta);
		this.AZ[1].enabled = !BlockDoor.IsDoorOpen(_blockValue.meta);
		this.AZ[2].enabled = (!flag2 && (flag3 || flag || _world.IsEditor()));
		this.AZ[3].enabled = (flag2 && (flag3 || _world.IsEditor()));
		this.AZ[4].enabled = ((!flag4 && flag5 && flag2) || flag3);
		return this.AZ;
	}
	
	private BlockActivationCommand[] AZ = new BlockActivationCommand[]
	{
		new BlockActivationCommand("open", "door", false),
		new BlockActivationCommand("close", "door", false),
		new BlockActivationCommand("lock", "lock", false),
		new BlockActivationCommand("unlock", "unlock", false),
		new BlockActivationCommand("keypad", "keypad", false)
	};
	
	// Block
	public override void OnBlockRemoved(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
	{
		if (!_blockValue.ischild)
		{
			this.removingDoor = true;
			if(script != null)
			{
				script.enabled = false;
				UnityEngine.Object.Destroy(script);
				BlockPoweredDoor.DebugMsg("script disabled and destroyed on block removed");
			}
			else
			BlockPoweredDoor.DebugMsg("script not found on block  removed");
			this.shape.OnBlockRemoved(_world, _chunk, _blockPos, _blockValue);
			if (this.isMultiBlock)
			{
				this.multiBlockPos.RemoveChilds(_world, _chunk.ClrIdx, _blockPos, _blockValue);
			}
		}
		else if (this.isMultiBlock)
		{
			this.multiBlockPos.RemoveParentBlock(_world, _chunk.ClrIdx, _blockPos, _blockValue);
		}
	}
	
	private void DisplayToolTipText(string str)
    {
		EntityPlayerLocal entity = GameManager.Instance.World.GetPrimaryPlayer();
		if (DateTime.Now > dteNextToolTipDisplayTime)
        {
            GameManager.ShowTooltip(entity, str);
			dteNextToolTipDisplayTime = DateTime.Now.AddSeconds(3);
        }
    }
	
	public override void OnBlockAdded(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
	{
		this.removingDoor = false;
		base.OnBlockAdded(_world, _chunk, _blockPos, _blockValue);
	}
}