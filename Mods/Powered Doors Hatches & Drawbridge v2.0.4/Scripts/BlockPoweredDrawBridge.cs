using System;
using UnityEngine;
using Audio;
using GUI_2;

public class BlockPoweredDrawBridge : BlockDrawBridge
{
	private BlockPoweredDrawBridge blockPoweredDrawBridge;
	private DateTime dteNextToolTipDisplayTime;
	private DoorAutoTrigger script;
	private Animator autoAnimator;
	private static bool showDebugLog = false;
	private bool removingDrawBridge;
	
	public static void DebugMsg(string msg)
	{
		if(showDebugLog)
		{
			Debug.Log(msg);
		}
	}
	
	private int BottomInputPos()
	{
		int bottomInputPos = 1;
		if (this.Properties.Values.ContainsKey("BottomInput"))
        {
			int.TryParse(this.Properties.Values["BottomInput"], out bottomInputPos);
        }
		return bottomInputPos;
	}
	
	public bool IsDoorControl(Block block)
	{
		bool isDoorControl = false;
		if (block.Properties.Values.ContainsKey("DoorController"))
        {
			bool.TryParse(block.Properties.Values["DoorController"], out isDoorControl);
        }
		return isDoorControl;
	}
	
	public bool IsNormallyOpened(WorldBase _world)
	{
		bool isNormallyOpened = false;
		if (!_world.IsRemote())
		{
			if (this.Properties.Values.ContainsKey("UpOnPowered"))
			{
				bool.TryParse(this.Properties.Values["UpOnPowered"], out isNormallyOpened);
			}
		}
		return isNormallyOpened;
	}
	
	public bool AutoTrigger()
	{
		bool isAutoTriggerOn = false;
		if (this.Properties.Values.ContainsKey("AutoTrigger"))
        {
			bool.TryParse(this.Properties.Values["AutoTrigger"], out isAutoTriggerOn);
        }
		return isAutoTriggerOn;
	}
	
	// Block
	public override void OnBlockEntityTransformBeforeActivated(WorldBase _world, Vector3i _blockPos, int _cIdx, BlockValue _blockValue, BlockEntityData _ebcd)
	{
		this.shape.OnBlockEntityTransformBeforeActivated(_world, _blockPos, _cIdx, _blockValue, _ebcd);
		if(AutoTrigger())
		{
			blockPoweredDrawBridge = this;
			if(_ebcd != null && _ebcd.bHasTransform)
			{
				GameObject gameObject = _ebcd.transform.gameObject;
				if(gameObject == null)
				{
					BlockPoweredDrawBridge.DebugMsg("gameObject null (OnBlockEntityTransformBeforeActivated)");
				}
				script = gameObject.GetComponent<DoorAutoTrigger>();
				if(script == null)
				{
					script = gameObject.AddComponent<DoorAutoTrigger>();
					if(script == null)
					{
						BlockPoweredDrawBridge.DebugMsg("Failed to add script");
						return;
					}
				}
				BlockPoweredDrawBridge.DebugMsg("Script found/added");
				script.enabled = true;
				script.blockPos = _blockPos;
				script.blockValue = _blockValue;
				script.cIdx = _cIdx;
				script.ebcd = ((World)_world).ChunkClusters[_cIdx].GetBlockEntity(script.blockPos);
				script.normallyOpened = IsNormallyOpened(_world);
			}
			else
			{
				BlockPoweredDrawBridge.DebugMsg("_ebcd is null (OnBlockEntityTransformBeforeActivated)");
				return;
			}
		}
	}
	
	public Animator DoorAnimator(BlockEntityData blockEntityData)
	{
		Animator doorAnimator = null;
		Animator[] componentsInChildren;
		if (blockEntityData != null && blockEntityData.bHasTransform && (componentsInChildren = blockEntityData.transform.GetComponentsInChildren<Animator>()) != null)
		{
			Animator[] array = componentsInChildren;
			for (int i = 0; i < array.Length; i++)
			{
				Animator animator = array[i];
				doorAnimator = animator;
			}
		}
		float animSpeed = 1;
		if (this.Properties.Values.ContainsKey("AnimationSpeed"))
		{
			float.TryParse(this.Properties.Values["AnimationSpeed"], out animSpeed);
		}
		doorAnimator.speed = animSpeed;
		autoAnimator = doorAnimator;
		return doorAnimator;
	}
	
	
	public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _cIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
	{
		if(AutoTrigger())
		{
			return false;
		}
		if (_blockValue.ischild)
		{
			Vector3i parentPos = Block.list[_blockValue.type].multiBlockPos.GetParentPos(_blockPos, _blockValue);
			BlockValue block = _world.GetBlock(parentPos);
			return this.OnBlockActivated(_indexInBlockActivationCommands, _world, _cIdx, parentPos, block, _player);
		}
		if(BlockPoweredDrawBridge.HasActivePower(_world, _cIdx, _blockPos))
		{
			BlockPoweredDrawBridge.DebugMsg("Block has active power.");
		}
		else
		{
			DisplayToolTipText("The door has no power.");
			Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/locked");
			return false;
		}
		TileEntitySecureDoor tileEntitySecureDoor = (TileEntitySecureDoor)_world.GetTileEntity(_cIdx, _blockPos);
		if (tileEntitySecureDoor == null && !_world.IsEditor())
		{
			return false;
		}
		bool flag = _world.IsEditor() ? ((_blockValue.meta & 4) != 0) : tileEntitySecureDoor.IsLocked();
		bool flag2 = !_world.IsEditor() && tileEntitySecureDoor.IsUserAllowed(GamePrefs.GetString(EnumGamePrefs.PlayerId));
		switch (_indexInBlockActivationCommands)
		{
		case 0:
			if (!_world.IsEditor() && flag && !flag2)
			{
				Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/locked");
				return false;
			}
			return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
		case 1:
			if (!_world.IsEditor() && flag && !flag2)
			{
				Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/locked");
				return false;
			}
			return this.OnBlockActivated(_world, _cIdx, _blockPos, _blockValue, _player);
		case 2:
			if (_world.IsEditor())
			{
				_blockValue.meta |= 4;
				_world.SetBlockRPC(_cIdx, _blockPos, _blockValue);
			}
			else
			{
				tileEntitySecureDoor.SetLocked(true);
			}
			Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/locking");
			GameManager.ShowTooltip(_player as EntityPlayerLocal, "doorLocked");
			return true;
		case 3:
			if (_world.IsEditor())
			{
				_blockValue.meta = (byte)((int)_blockValue.meta & -5);
				_world.SetBlockRPC(_cIdx, _blockPos, _blockValue);
			}
			else
			{
				tileEntitySecureDoor.SetLocked(false);
			}
			Manager.BroadcastPlayByLocalPlayer(_blockPos.ToVector3() + Vector3.one * 0.5f, "Misc/unlocking");
			GameManager.ShowTooltip(_player as EntityPlayerLocal, "doorUnlocked");
			return true;
		case 4:
		{
			LocalPlayerUI uIForPlayer = LocalPlayerUI.GetUIForPlayer(_player as EntityPlayerLocal);
			if (uIForPlayer != null)
			{
				XUiC_KeypadWindow.Open(uIForPlayer, tileEntitySecureDoor);
			}
			return true;
		}
		default:
			return false;
		}
	}
	
	public static bool HasActivePower(WorldBase _world, int _cIdx, Vector3i _blockPos)
	{
		if(_world == null)
		{
			_world = GameManager.Instance.World;
			if(_world == null)
			{
				BlockPoweredDrawBridge.DebugMsg("_world is still null (HasActivePower)");
			}
		}
		BlockValue blockValue = GameManager.Instance.World.GetBlock(_blockPos);
		Block block = Block.list[blockValue.type];
		BlockPoweredDrawBridge doorBlock = block as BlockPoweredDrawBridge;
		if(doorBlock == null)
		{
			// DONT REMOVE THIS RETURN, THIS STOPS NULL REF ON REMOVE
			BlockPoweredDrawBridge.DebugMsg("block as BlockPoweredDrawBridge is null, most likely the door has been removed(HasActivePower)");
			// the door has been removed
			return false;
		}
		if(doorBlock.removingDrawBridge)
		{
			return false;
		}
		Vector3i newBlockPos = new Vector3i(_blockPos.x,_blockPos.y-doorBlock.BottomInputPos(),_blockPos.z);
		Vector3i inputPosA = newBlockPos;
		Vector3i inputPosB = newBlockPos;
		Vector3i inputPosC = newBlockPos;
		Vector3i inputPosD = newBlockPos;
		if(blockValue.rotation == 0) // hinges to the north
		{
			inputPosA = new Vector3i(newBlockPos.x,newBlockPos.y,newBlockPos.z+2); // 2nd FROM RIGHT
			inputPosB = new Vector3i(newBlockPos.x+1,newBlockPos.y,newBlockPos.z+2); // 1st FROM RIGHT
			inputPosC = new Vector3i(newBlockPos.x-1,newBlockPos.y,newBlockPos.z+2); // 3rd FROM RIGHT
			inputPosD = new Vector3i(newBlockPos.x-2,newBlockPos.y,newBlockPos.z+2); // 4th FROM RIGHT
		}
		if(blockValue.rotation == 2) // hinges to the south
		{
			inputPosA = new Vector3i(newBlockPos.x,newBlockPos.y,newBlockPos.z-2);// 2nd FROM RIGHT
			inputPosB = new Vector3i(newBlockPos.x-1,newBlockPos.y,newBlockPos.z-2);  // 1st FROM RIGHT
			inputPosC = new Vector3i(newBlockPos.x+1,newBlockPos.y,newBlockPos.z-2); // 3rd FROM RIGHT
			inputPosD = new Vector3i(newBlockPos.x+2,newBlockPos.y,newBlockPos.z-2); // 4th FROM RIGHT
		}
		if(blockValue.rotation == 3) // hinges to the west
		{
			inputPosA = new Vector3i(newBlockPos.x-2,newBlockPos.y,newBlockPos.z); // 2nd FROM RIGHT
			inputPosB = new Vector3i(newBlockPos.x-2,newBlockPos.y,newBlockPos.z+1); // 1st FROM RIGHT
			inputPosC = new Vector3i(newBlockPos.x-2,newBlockPos.y,newBlockPos.z-1); // 3rd FROM RIGHT
			inputPosD = new Vector3i(newBlockPos.x-2,newBlockPos.y,newBlockPos.z-2); // 4th FROM RIGHT
		}
		
		if(blockValue.rotation == 1) // hinges to the east
		{
			inputPosA = new Vector3i(newBlockPos.x+2,newBlockPos.y,newBlockPos.z); // 2nd FROM RIGHT
			inputPosB = new Vector3i(newBlockPos.x+2,newBlockPos.y,newBlockPos.z-1); // 1st FROM RIGHT
			inputPosC = new Vector3i(newBlockPos.x+2,newBlockPos.y,newBlockPos.z+1); // 3rd FROM RIGHT
			inputPosD = new Vector3i(newBlockPos.x+2,newBlockPos.y,newBlockPos.z+2); // 4th FROM RIGHT
		}
		Vector3i[] locations = new Vector3i[4];
		locations[0] = inputPosA;
		locations[1] = inputPosB;
		locations[2] = inputPosC;
		locations[3] = inputPosD;
		foreach (Vector3i inputPos in locations)
		{
			BlockValue inputBlockValue = _world.GetBlock(inputPos);
			Block inputBlock = Block.list[inputBlockValue.type];
			Type inputBlockType = Block.list[inputBlockValue.type].GetType();
			if(inputBlockType == typeof(BlockPowered))
			{
				TileEntityPowered tileEntityPowered = (TileEntityPowered)_world.GetTileEntity(_cIdx, inputPos);
				if (tileEntityPowered != null)
				{
					if(tileEntityPowered.IsPowered)
					{
						if(doorBlock.IsDoorControl(inputBlock))
						{
							return true;
						}
					}
				}
			}
		}
		return false;
	}
	
	public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
	{
		if(AutoTrigger())
		{
			return "Testing";
		}
		else
		{
			if (_blockValue.ischild)
			{
				Vector3i parentPos = Block.list[_blockValue.type].multiBlockPos.GetParentPos(_blockPos, _blockValue);
				BlockValue block = _world.GetBlock(parentPos);
				return this.GetActivationText(_world, block, _clrIdx, parentPos, _entityFocusing);
			}
			TileEntitySecureDoor tileEntitySecureDoor = (TileEntitySecureDoor)_world.GetTileEntity(_clrIdx, _blockPos);
			if (tileEntitySecureDoor == null && !_world.IsEditor())
			{
				return string.Empty;
			}
			bool flag = _world.IsEditor() ? ((_blockValue.meta & 4) != 0) : tileEntitySecureDoor.IsLocked();
			PlayerActionsLocal playerInput = ((EntityPlayerLocal)_entityFocusing).playerInput;
			string keybindString = UIUtils.GetKeybindString(playerInput.Activate, playerInput.PermanentActions.Activate);
			string arg = Localization.Get("door", string.Empty);
			if (!flag)
			{
				return string.Format(Localization.Get("tooltipUnlocked", string.Empty), keybindString, arg);
			}
			return string.Format(Localization.Get("tooltipLocked", string.Empty), keybindString, arg);
		}
		
	}
	
	public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
	{
		if(AutoTrigger())
		{
			return new BlockActivationCommand[0];
		}
		if (_blockValue.ischild)
		{
			Vector3i parentPos = Block.list[_blockValue.type].multiBlockPos.GetParentPos(_blockPos, _blockValue);
			BlockValue block = _world.GetBlock(parentPos);
			return this.GetBlockActivationCommands(_world, block, _clrIdx, parentPos, _entityFocusing);
		}
		TileEntitySecureDoor tileEntitySecureDoor = (TileEntitySecureDoor)_world.GetTileEntity(_clrIdx, _blockPos);
		if (tileEntitySecureDoor == null && !_world.IsEditor())
		{
			return new BlockActivationCommand[0];
		}
		string @string = GamePrefs.GetString(EnumGamePrefs.PlayerId);
		PersistentPlayerData persistentPlayerData = _world.IsEditor() ? null : _world.GetGameManager().GetPersistentPlayerList().GetPlayerData(tileEntitySecureDoor.GetOwner());
		bool flag = _world.IsEditor() || (!tileEntitySecureDoor.IsOwner(@string) && (persistentPlayerData != null && persistentPlayerData.ACL != null) && persistentPlayerData.ACL.Contains(@string));
		bool flag2 = _world.IsEditor() ? ((_blockValue.meta & 4) != 0) : tileEntitySecureDoor.IsLocked();
		bool flag3 = _world.IsEditor() || tileEntitySecureDoor.IsOwner(@string);
		bool flag4 = !_world.IsEditor() && tileEntitySecureDoor.IsUserAllowed(@string);
		bool flag5 = !_world.IsEditor() && tileEntitySecureDoor.HasPassword();
		this.AZ[0].enabled = BlockDoor.IsDoorOpen(_blockValue.meta);
		this.AZ[1].enabled = !BlockDoor.IsDoorOpen(_blockValue.meta);
		this.AZ[2].enabled = (!flag2 && (flag3 || flag || _world.IsEditor()));
		this.AZ[3].enabled = (flag2 && (flag3 || _world.IsEditor()));
		this.AZ[4].enabled = ((!flag4 && flag5 && flag2) || flag3);
		return this.AZ;
	}
	
	private BlockActivationCommand[] AZ = new BlockActivationCommand[]
	{
		new BlockActivationCommand("open", "door", false),
		new BlockActivationCommand("close", "door", false),
		new BlockActivationCommand("lock", "lock", false),
		new BlockActivationCommand("unlock", "unlock", false),
		new BlockActivationCommand("keypad", "keypad", false)
	};
	
	// Block
	public override void OnBlockRemoved(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
	{
		if (!_blockValue.ischild)
		{
			this.removingDrawBridge = true;
			if(script != null)
			{
				script.enabled = false;
				BlockPoweredDrawBridge.DebugMsg("script disabled on block removed");
			}
			else
			BlockPoweredDrawBridge.DebugMsg("script not found on block  removed");
			this.shape.OnBlockRemoved(_world, _chunk, _blockPos, _blockValue);
			if (this.isMultiBlock)
			{
				this.multiBlockPos.RemoveChilds(_world, _chunk.ClrIdx, _blockPos, _blockValue);
			}
		}
		else if (this.isMultiBlock)
		{
			this.multiBlockPos.RemoveParentBlock(_world, _chunk.ClrIdx, _blockPos, _blockValue);
		}
	}
	
	private void DisplayToolTipText(string str)
    {
		EntityPlayerLocal entity = GameManager.Instance.World.GetPrimaryPlayer();
		if (DateTime.Now > dteNextToolTipDisplayTime)
        {
            GameManager.ShowTooltip(entity, str);
			dteNextToolTipDisplayTime = DateTime.Now.AddSeconds(3);
        }
    }
	
	public override void OnBlockAdded(WorldBase _world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
	{
		this.removingDrawBridge = false;
		base.OnBlockAdded(_world, _chunk, _blockPos, _blockValue);
	}
}