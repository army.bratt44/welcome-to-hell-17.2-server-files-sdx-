using System;
using UnityEngine;
using System.Linq;
using System.Collections.Generic;

public class BlockMultiLevelElevatorFloor : BlockPowered
{
	BlockMultiLevelElevatorGround groundFloorBlock;
	Vector3i groundBlockPos;
	BlockValue groundBlockValue;
	bool groundNotLinked;
	
	public Vector3 leftShaftStartPos;
	public Vector3 leftShaftEndPos;
	public Vector3 rightShaftStartPos;
	public Vector3 rightShaftEndPos;
	
	public override bool OnBlockActivated(int _indexInBlockActivationCommands, WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, EntityAlive _player)
	{
		if(BlockMultiLevelElevatorGround.LookRay(this, _blockValue, _player) == "Call Elevator")
		{
			string key = _blockPos.x + "_" + _blockPos.z + "_Ground";
			DebugMsg("Call button _blockPos: " + _blockPos);
			BlockMultiLevelElevatorGround.ButtonClick(_blockPos);
			if (BlockMultiLevelElevatorGround.groundFloors.ContainsKey(key))
			{
				BlockEntityData callFloor_ebcd = _world.ChunkClusters[_clrIdx].GetBlockEntity(_blockPos);
				if(callFloor_ebcd != null && callFloor_ebcd.bHasTransform)
				{
					LinkToGround(_world, key);
					if(this.groundFloorBlock != null)
					{
						
						if(BlockMultiLevelElevatorGround.RequiresPower(this) && !BlockMultiLevelElevatorGround.isBlockPoweredUp(_blockPos, _clrIdx))
						{
							ToolTipText("Floor Needs power");
							return true;
						}
						else
						{
							this.groundFloorBlock.CallButtonActivated(_world, _clrIdx, this.groundBlockPos, this.groundBlockValue, callFloor_ebcd, false);
						}
					}
				}
			}
			else
			{
				DebugMsg("No ground floor found (call button)");
			}
		}
		return true;
	}
	
	public override string GetActivationText(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
	{
		
		string text = "";
		if(BlockMultiLevelElevatorGround.LookRay(this, _blockValue, _entityFocusing) == "Call Elevator")
		{
			text = "Call Elevator" + " (" + BlockMultiLevelElevatorGround.GetFloorNumber(_blockPos) + ")";
			if(this.groundFloorBlock != null)
			{
				if(this.groundFloorBlock.elevatorController != null)
				{
					BlockEntityData _ebcd = _world.ChunkClusters[_clrIdx].GetBlockEntity(_blockPos);
					if(_ebcd != null && _ebcd.bHasTransform)
					{
						if(this.groundFloorBlock.elevatorController.liftPlatform != null)
						{
							if(this.groundFloorBlock.elevatorController.liftPlatform.position.y == _ebcd.transform.position.y)
							{
								text = "Floor " + BlockMultiLevelElevatorGround.GetFloorNumber(_blockPos);
							}
							else
							{
								// DebugMsg("positions dont match");
							}
						}
					}
					else
					{
						text = "Ground Floor Not Found";
					}
				}
				else
				{
					// DebugMsg("No controller script");
				}
			}
			else
			{
				text = "Ground Floor Not Found";
				string key = _blockPos.x + "_" + _blockPos.z + "_Ground";
				if (BlockMultiLevelElevatorGround.groundFloors.ContainsKey(key))
				{
					LinkToGround(_world, key);
				}
			}
		}
		return text;
	}
	
	private BlockActivationCommand[] SZ = new BlockActivationCommand[]
	{
		new BlockActivationCommand("take", "hand", true)
	};
	
	public override BlockActivationCommand[] GetBlockActivationCommands(WorldBase _world, BlockValue _blockValue, int _clrIdx, Vector3i _blockPos, EntityAlive _entityFocusing)
	{
		return this.SZ;
	}
	
	void LinkToGround(WorldBase _world, string key)
	{
		Vector3i checkedVector3i = default(Vector3i);
		if (BlockMultiLevelElevatorGround.groundFloors.TryGetValue(key, out checkedVector3i))
		{
			DebugMsg("(Top)Ground Floor For: " + key + "Saved Floor Vector3i: " + checkedVector3i);
			this.groundBlockPos = checkedVector3i;
			this.groundBlockValue = _world.GetBlock(checkedVector3i);
			Block block = Block.list[this.groundBlockValue.type];
			Type blockType = Block.list[this.groundBlockValue.type].GetType();
			if(blockType == typeof(BlockMultiLevelElevatorGround))
			{
				this.groundFloorBlock = block as BlockMultiLevelElevatorGround;
			}
		}
	}
	
	public override void OnBlockEntityTransformAfterActivated(WorldBase _world, Vector3i _blockPos, int _cIdx, BlockValue _blockValue, BlockEntityData _ebcd)
	{
		base.OnBlockEntityTransformAfterActivated(_world, _blockPos, _cIdx, _blockValue, _ebcd);
		if(!String.IsNullOrEmpty(BlockMultiLevelElevatorGround.ShaftDoor1Transform(this)))
		{
			Transform leftShaftDoorTransform = BlockMultiLevelElevatorGround.FindTransformNamed(_ebcd, BlockMultiLevelElevatorGround.ShaftDoor1Transform(this));
			if(leftShaftDoorTransform != null && leftShaftDoorTransform != default(Transform))
			{
				this.leftShaftStartPos = leftShaftDoorTransform.localPosition;
				this.leftShaftEndPos = leftShaftDoorTransform.localPosition + BlockMultiLevelElevatorGround.GetShaftDoorOffSet(this, 1);
				if(!MultiLevelElevatorController.IsAtThisFloor(_blockValue.meta))
				{
					leftShaftDoorTransform.localPosition =  leftShaftDoorTransform.localPosition + BlockMultiLevelElevatorGround.GetShaftDoorOffSet(this, 1);
				}
			}
		}
		if(!String.IsNullOrEmpty(BlockMultiLevelElevatorGround.ShaftDoor2Transform(this)))
		{
			Transform rightShaftDoorTransform = BlockMultiLevelElevatorGround.FindTransformNamed(_ebcd, BlockMultiLevelElevatorGround.ShaftDoor2Transform(this));
			if(rightShaftDoorTransform != null && rightShaftDoorTransform != default(Transform))
			{
				this.rightShaftStartPos = rightShaftDoorTransform.localPosition;
				this.rightShaftEndPos =  rightShaftDoorTransform.localPosition + BlockMultiLevelElevatorGround.GetShaftDoorOffSet(this, 2);
				if(!MultiLevelElevatorController.IsAtThisFloor(_blockValue.meta))
				{
					rightShaftDoorTransform.localPosition =  rightShaftDoorTransform.localPosition + BlockMultiLevelElevatorGround.GetShaftDoorOffSet(this, 2);
				}
			}
		}
		string floorsKey = _blockPos.x + "_" + _blockPos.z + "_floorsList";
		if(!BlockMultiLevelElevatorGround.floors.ContainsKey(floorsKey))
		{
			BlockMultiLevelElevatorGround.floors.Add(floorsKey, new List<Vector3i>());
		}
		if(BlockMultiLevelElevatorGround.floors.ContainsKey(floorsKey))
		{
			List<Vector3i> list = new List<Vector3i>();
			if (BlockMultiLevelElevatorGround.floors.TryGetValue(floorsKey, out list))
			{
				bool addFloor = true;
				Vector3i[] floorVectorArray = list.ToArray();
				foreach(Vector3i arrayFloor in floorVectorArray)
				{
					DebugMsg("new foreach");
					if(arrayFloor.y == _blockPos.y)
					{
						addFloor = false;
						break;
					}
				}	
				if(addFloor)
				{
					list.Add(_blockPos);
				}
			}
		}
		string key = _blockPos.x + "_" + _blockPos.z + "_Ground";
		if (BlockMultiLevelElevatorGround.groundFloors.ContainsKey(key))
		{
			LinkToGround(_world, key);
		}
		else
		{
			// DebugMsg("No ground floor found (AfterActivated)");
			return;
		}
		BlockEntityData ground_ebcd = _world.ChunkClusters[_cIdx].GetBlockEntity(this.groundBlockPos);
		Transform groundFloorTransform = ground_ebcd.transform;
		if(groundFloorTransform != null && groundFloorTransform != default(Transform))
		{
			DebugMsg("Found groundFloorTransform on reload");
			Transform elevatorTransform = FindTransform(groundFloorTransform, BlockMultiLevelElevatorGround.PlatformBaseTransform(this.groundFloorBlock));
			MultiLevelElevatorController elevatorController = elevatorTransform.gameObject.GetComponent<MultiLevelElevatorController>();
			if(elevatorController != null)
			{
				DebugMsg("Found elevatorController on reload");
				if(MultiLevelElevatorController.IsAtThisFloor(_blockValue.meta))
				{
					DebugMsg("elevator position needs forcing on reload");
					Transform liftPlatform = FindTransform(groundFloorTransform, BlockMultiLevelElevatorGround.TransformToMove(this.groundFloorBlock));
					if(liftPlatform != null && liftPlatform != default(Transform))
					{
						DebugMsg("Forcing elevator position to up on reload");
						liftPlatform.position = new Vector3(liftPlatform.position.x, _ebcd.transform.position.y, liftPlatform.position.z);
					}
					else
						DebugMsg("could NOT find liftPlatform transform on reload");
				}
				else
					DebugMsg("elevator position doesnt need forcing on reload");
			}
			else
				DebugMsg("could NOT find elevatorController on reload");
		}
		else
			DebugMsg("could NOT find groundFloorTransform transform on reload");
	}
	
	// should be using the one in control floor and get rid of this
	public static Transform FindTransform(Transform _groundFloorTransform, string _name)
	{
		Transform[] transforms = _groundFloorTransform.GetComponentsInChildren<Transform>();
		foreach (Transform newTransform in transforms)
		{
			if(newTransform.gameObject.name == _name)
			{
				DebugMsg("FindTransformNamed match found: " + newTransform.gameObject.name);
				return newTransform;
			}
		}
		return default(Transform);
	}
	
	public override void OnBlockRemoved(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
	{
		
		
		BlockEntityData _ebcd = world.ChunkClusters[_chunk.ClrIdx].GetBlockEntity(this.groundBlockPos);
		if(_ebcd == null || !_ebcd.bHasTransform)
		{
			DebugMsg("OnBlockRemoved: _ebcd null");
			base.OnBlockRemoved(world, _chunk, _blockPos, _blockValue);
			return;
		}
		if (!_blockValue.ischild)
		{
			int floorNumber = BlockMultiLevelElevatorGround.GetFloorNumber(_blockPos);
			int floorCount = BlockMultiLevelElevatorGround.FloorCount(_blockPos);
			int newFloorTarget = floorNumber-1;
			if(floorNumber == 1)
			{
				newFloorTarget = floorNumber;
			}
			BlockEntityData this_ebcd = world.ChunkClusters[_chunk.ClrIdx].GetBlockEntity(_blockPos);
			string floorsKey = _blockPos.x + "_" + _blockPos.z + "_floorsList";
			if(BlockMultiLevelElevatorGround.floors.ContainsKey(floorsKey))
			{
				List<Vector3i> newFloorsList = new List<Vector3i>();
				List<Vector3i> list = new List<Vector3i>();
				if (BlockMultiLevelElevatorGround.floors.TryGetValue(floorsKey, out list))
				{
					foreach(Vector3i floor in list.ToList())
					{
						if(floor == _blockPos)
						{
							list.Remove(floor);
						}
					}
				}
				bool debugFloorList = false;
				if(debugFloorList)
				{
					List<Vector3i> newList = new List<Vector3i>();
					bool wasRemoved = true;
					if (BlockMultiLevelElevatorGround.floors.TryGetValue(floorsKey, out newList))
					{
						foreach(Vector3i floor in newList.ToList())
						{
							if(floor == _blockPos)
							{
								wasRemoved = false;
							}
						}
						if(wasRemoved)
						{
							DebugMsg("Floor was removed from the list");
						}
						else
						{
							DebugMsg("Error floor is still in list");
						}
					}
				}
			}
			bool thisFloorIsTarget = false;
			if(this.groundFloorBlock != null)
			{
				if(this.groundFloorBlock.elevatorController != null)
				{
					if(this.groundFloorBlock.elevatorController.moveElevator && this.groundFloorBlock.elevatorController.targetPos3i == _blockPos)
					{
						this.groundFloorBlock.elevatorController.moveElevator = false;
						BlockValue startBlockValue = world.GetBlock(this.groundFloorBlock.elevatorController.startPos3i);
						if(!startBlockValue.ischild)
						{
							thisFloorIsTarget = true;
							startBlockValue.meta = (byte) (startBlockValue.meta & ~(1 << 1));
							world.SetBlockRPC(this.groundFloorBlock.elevatorController.startPos3i, startBlockValue);
						}
					}
				}
			}
			if(MultiLevelElevatorController.IsAtThisFloor(_blockValue.meta) || thisFloorIsTarget)
			{
				if(this.groundFloorBlock != null)
				{
					Transform groundFloorTransform = _ebcd.transform;
					if(groundFloorTransform != null && groundFloorTransform != default(Transform))
					{
						Transform liftPlatform = FindTransform(groundFloorTransform, BlockMultiLevelElevatorGround.TransformToMove(this.groundFloorBlock));
						if(liftPlatform != null && liftPlatform != default(Transform))
						{
							DebugMsg("Forcing elevator position to ground");
							// could look up the closest elevator here and send it there instead of the just up or down
							_blockValue.meta = (byte) (_blockValue.meta & ~(1 << 1));
							world.SetBlockRPC(_blockPos, _blockValue);
							BlockMultiLevelElevatorGround.GetSetSelectedFloor(_blockPos, newFloorTarget, false);
							EntityAlive player = world.GetPrimaryPlayer();
							Vector3i targetPos3i = BlockMultiLevelElevatorGround.TargetVector3i(newFloorTarget, _blockPos.x, _blockPos.z, false);
							if(targetPos3i != default(Vector3i))
							{
								BlockEntityData targetEbcd = world.ChunkClusters[_chunk.ClrIdx].GetBlockEntity(targetPos3i);
								if(targetEbcd != null && targetEbcd.bHasTransform)
								{
									this.groundFloorBlock.CallButtonActivated(world, _chunk.ClrIdx, this.groundBlockPos, this.groundBlockValue, targetEbcd, true);
								}
							}
						}
					}
				}
			}
		}
		base.OnBlockRemoved(world, _chunk, _blockPos, _blockValue);
		return;
	}
	
	public override void OnBlockAdded(WorldBase world, Chunk _chunk, Vector3i _blockPos, BlockValue _blockValue)
	{
		base.OnBlockAdded(world, _chunk, _blockPos, _blockValue);
		if (!_blockValue.ischild)
		{
			// // IsAtThisFloor false
			_blockValue.meta = (byte) (_blockValue.meta & ~(1 << 1));
			world.SetBlockRPC(_blockPos, _blockValue);
		}
	}
	
	public static int MaxFloorCount(Block _block)
	{
		int maxFloorCount = 0;
		if (_block.Properties.Values.ContainsKey("MaxFloorCount"))
		{
			int.TryParse(_block.Properties.Values["MaxFloorCount"], out maxFloorCount);
		}
		return maxFloorCount;
	}
	
	public override bool CanPlaceBlockAt(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue, bool _bOmitCollideCheck)
	{
		bool canPlaceFloor = true;
		if(MaxFloorCount(this) != 0 && MaxFloorCount(this) <= BlockMultiLevelElevatorGround.FloorCount(_blockPos))
		{
			canPlaceFloor = false;
			BlockMultiLevelElevatorGround.ToolTipTextOnClick("Max Floor Count Reached");
		}
		return _blockPos.y <= 253 && (GameManager.Instance.IsEditMode() || canPlaceFloor && !((World)_world).IsWithinTraderArea(_blockPos)) && (!Block.list[_blockValue.type].isMultiBlock || _blockPos.y + Block.list[_blockValue.type].multiBlockPos.dim.y < 254) && (GameManager.Instance.IsEditMode() || _bOmitCollideCheck || !this.overlapsWithOtherBlock(_world, _clrIdx, _blockPos, _blockValue));
	}
	
	private bool overlapsWithOtherBlock(WorldBase _world, int _clrIdx, Vector3i _blockPos, BlockValue _blockValue)
	{
		if (!this.isMultiBlock)
		{
			int type = _world.GetBlock(_clrIdx, _blockPos).type;
			return type != 0 && !Block.list[type].blockMaterial.IsGroundCover && !Block.list[type].blockMaterial.IsLiquid;
		}
		byte rotation = _blockValue.rotation;
		for (int i = this.multiBlockPos.Length - 1; i >= 0; i--)
		{
			int type2 = _world.GetBlock(_clrIdx, _blockPos + this.multiBlockPos.Get(i, _blockValue.type, (int)rotation)).type;
			if (type2 != 0 && !Block.list[type2].blockMaterial.IsGroundCover && !Block.list[type2].blockMaterial.IsLiquid)
			{
				return true;
			}
		}
		return false;
	}
	
	private void ToolTipText(string str)
    {
		EntityPlayerLocal entity = GameManager.Instance.World.GetPrimaryPlayer();
		DateTime dteNextToolTipDisplayTime = default(DateTime);
        if (DateTime.Now > dteNextToolTipDisplayTime)
        {
            GameManager.ShowTooltip(entity, str);
			dteNextToolTipDisplayTime = DateTime.Now.AddSeconds(3);
        }
    }
	
	public static void DebugMsg(string msg)
	{
		bool showDebugLog = false;
		if(showDebugLog)
		{
			Debug.Log(msg);
		}
	}
}